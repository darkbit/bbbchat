package sockjs

import (
	"math/rand"
	"context"
	"fmt"
	"net/url"
	"time"
	"encoding/json"

	"bbbchatrepo/util"
	"nhooyr.io/websocket"
)

type Client struct {
	socket *websocket.Conn
	Rx chan interface{}
	Tx chan interface{}
}

func Connect(baseURL url.URL) (*Client, error) {
	baseURL.Path += fmt.Sprintf("/%03d/%s/websocket", rand.Intn(1000), util.RandString())
	baseURL.Scheme = "wss"
	ctx, _ := context.WithTimeout(context.TODO(), 5 * time.Second)
	socket, _, err := websocket.Dial(ctx, baseURL.String(), nil)
	if err != nil {
		return nil, err
	}
	ret := &Client {
		socket: socket,
		Rx: make(chan interface{}, 10),
		Tx: make(chan interface{}),
	}
	go ret.startReading()
	go ret.startSending()
	return ret, nil
}

func (client *Client) startSending() {
	for pkg := range client.Tx {
		toSend, err := json.Marshal([]interface{} { pkg })
		if err != nil {
			panic(err)
		}
		if err := client.socket.Write(context.TODO(), websocket.MessageText, toSend); err != nil {
			fmt.Println("Couldn't send sockjs message:", err)
		}
	}
}

func (client *Client) startReading() {
	defer close(client.Rx)
	defer close(client.Tx)

	frameLoop: for {
		_, msg, err := client.socket.Read(context.TODO())
		if err != nil {
			fmt.Println("Got an error while reading the websocket:", err)
			break
		}
		if len(msg) == 0 {
			continue
		}

		switch rune(msg[0]) {
			case 'o', 'h': // opening and heartbeat
			case 'c':
				// Close the connection
				client.socket.Close(websocket.StatusNormalClosure, "bye")
				break frameLoop
			case 'a':
				var data []interface{}
				if err := json.Unmarshal(msg[1:], &data); err != nil {
					fmt.Println("Got an invalid sockjs array packet:", err)
					continue frameLoop
				}
				for _, elem := range data {
					client.Rx <- elem
				}
			default:
				fmt.Println("Got invalid sockjs packet:", msg)
		}
	}
}
