package main

import (
	"net/http"
	"os"
	"crypto/tls"

	"bbbchatrepo/bbb"
)


func main() {
	file, _ := os.OpenFile("/tmp/keyfile", os.O_APPEND | os.O_CREATE | os.O_WRONLY, 0644)
	defer file.Close()
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config {
		KeyLogWriter: file,
	}

	bbb.Do()

	<-make(chan int)
}
