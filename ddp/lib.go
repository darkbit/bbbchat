package ddp

// See https://github.com/meteor/meteor/blob/devel/packages/ddp/DDP.md

import (
	"fmt"
	"encoding/json"

	"bbbchatrepo/util"
)

type PacketReaderWriter interface {
	ReadPacket() []byte
	WritePacket([]byte)
}

type Datastore interface {
	Add(collection string, id string, fields interface{})
}

type Conn struct {
	pkts PacketReaderWriter
	registerMethodReply chan chan interface{}
	Store Datastore
}

func Connect(pkts PacketReaderWriter, store Datastore) (*Conn, error) {
	ret := &Conn{
		pkts: pkts,
		registerMethodReply: make(chan chan interface{}, 100),
		Store: store,
	}
	if err := ret.connect(); err != nil {
		return nil, err
	}

	go ret.packetHandler()

	return ret, nil
}

type message struct {
	Msg string `json:"msg"`

	// ============== Establishing ============
	// connect
	Session string `json:"session,omitempty"`
	Version string `json:"version,omitempty"`
	Support []string `json:"support,omitempty"`

	// connected
	// Session

	// failed
	// Version

	// ============= Hearthbeats  =============
	// ping
	// Id

	// pong
	// Id

	// ============= Managing data =============
	// sub
	Id string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Params []interface{} `json:"params,omitempty"`

	// unsub
	// Id

	// nosub
	// Id
	Error interface{} `json:"error,omitempty"`

	// added
	Collection string `json:"collection,omitempty"`
	// Id
	Fields interface{} `json:"fields,omitempty"`

	// changed
	// Collection
	// Id
	// Fields
	Cleared []string `json:"cleared,omitempty"`

	// removed
	// Collection
	// Id

	// ready
	Subs []string `json:"subs,omitempty"`

	// addedBefore
	// Collection
	// Id
	// Fields
	Before *string `json:"before,omitempty"`

	// movedBefore
	// Collection
	// Id
	// Before


	// =============== RPCs ==================
	// method
	Method string `json:"method,omitempty"`
	// Params
	// Id
	RandomSeed interface{} `json:"randomSeed,omitempty"`

	// result
	// Id
	// Error
	Result interface{} `json:"result,omitempty"`

	// updated
	Methods []string `json:"methods,omitempty"`
}

func (conn *Conn) packetHandler() {
	methodReplies := make(map[string]chan interface{})
	for {
		msg := conn.readMessage()

		methodRegisterLoop: for {
			select {
				case channel := <-conn.registerMethodReply:
					id := (<-channel).(string)
					methodReplies[id] = channel
				default:
					break methodRegisterLoop
			}
		}

		// Handle messages
		switch msg.Msg {
			case "result":
				replyChan, ok := methodReplies[msg.Id]
				if !ok {
					fmt.Println("Got unknown method reply", msg.Id)
					break
				}
				if msg.Error != nil {
					replyChan <- fmt.Errorf("Method Error: %#v", msg.Error)
				} else {
					replyChan <- msg.Result
				}
			case "added":
				if store := conn.Store; store != nil {
					store.Add(msg.Collection, msg.Id, msg.Fields)
				}
			case "ping":
				conn.sendMessage(message{
					Msg: "pong",
					Id: msg.Id,
				})
		}
	}
}

func (conn *Conn) sendMessage(msg message) {
	encoded, err := json.Marshal(&msg)
	if err != nil {
		panic(err)
	}

	conn.pkts.WritePacket(encoded)
}

func (conn *Conn) readMessage() message {
	var msg message
	for {
		if err := json.Unmarshal(conn.pkts.ReadPacket(), &msg); err == nil {
			break
		} else {
			fmt.Println("Error decoding DDP message:", err)
		}
	}

	return msg
}

func (conn *Conn) connect() error {
	conn.sendMessage(message{
		Msg: "connect",
		Version: "1",
		Support: []string { "1" },
	})

	waitForConnected: for {
		msg := conn.readMessage()
		switch msg.Msg {
			case "connected":
				break waitForConnected
			case "failed":
				return fmt.Errorf("Meteor connection failed")
			default:
				fmt.Println("Got unknown message:", msg)
		}
	}

	return nil
}

func (conn *Conn) CallMethod(name string, args ...interface{}) interface{} {
	id := util.RandString()

	replyChan := make(chan interface{}) // No buffering, otherwise we read our own id, we sent
	conn.registerMethodReply <- replyChan

	conn.sendMessage(message{
		Msg: "method",
		Method: name,
		Params: args,
		Id: id,
	})
	replyChan <- id
	reply := <-replyChan

	return reply
}

func (conn *Conn) Subscribe(name string) {
	conn.sendMessage(message{
		Msg: "sub",
		Id: util.RandString(),
		Name: name,
	})
}
