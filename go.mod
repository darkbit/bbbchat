module bbbchatrepo

go 1.15

require (
	github.com/esiqveland/notify v0.9.1
	github.com/godbus/dbus/v5 v5.0.3
	nhooyr.io/websocket v1.8.6
)
