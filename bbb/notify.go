package bbb

import (
	"fmt"

	"github.com/esiqveland/notify"
	"github.com/godbus/dbus/v5"
)

type Notifier struct {
	dbus *dbus.Conn
}

func (not *Notifier) initLazy() {
	if not.dbus != nil {
		return
	}

	var err error
	not.dbus, err = dbus.SessionBusPrivate()
	if err != nil {
		panic(err)
	}

	if err = not.dbus.Auth(nil); err != nil {
		panic(err)
	}

	if err = not.dbus.Hello(); err != nil {
		panic(err)
	}
}

func (not *Notifier) SendMessage(msg string) {
	not.initLazy()

	n := notify.Notification{
		AppName:       "BigBlueButton Chat",
		Summary:       "BigBlueButton Chatmessage",
		Body:          msg,
		ExpireTimeout: 5000,
	}

	if _, err := notify.SendNotification(not.dbus, n); err != nil {
		fmt.Println("error sending notification:", err)
	}
}
