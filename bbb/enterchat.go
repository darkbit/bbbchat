package bbb

import (
	"encoding/json"
	"fmt"
	"net/http"
	urllib "net/url"
)


type EnterChatResp struct {
        MeetingID string
        ExternUserID string
        InternalUserID string
        AuthToken string
}

func EnterChat(url urllib.URL, sessionToken SessionToken) (EnterChatResp, error) {
	url.Path = "/bigbluebutton/api/enter"
	url.RawQuery = urllib.Values { "sessionToken": { sessionToken } }.Encode()
	resp, err := http.Get(url.String())
	if err != nil {
		return EnterChatResp{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return EnterChatResp{}, fmt.Errorf("Unexpected status to enter api call: %s", resp.Status)
	}

	var chatresp struct {
		Response EnterChatResp
	}
	if err := json.NewDecoder(resp.Body).Decode(&chatresp); err != nil {
		return EnterChatResp{}, err
	}

	return chatresp.Response, nil
}
