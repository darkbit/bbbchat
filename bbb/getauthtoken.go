package bbb

import (
	"net/http"
	"strings"
	"io/ioutil"
	"fmt"
)

func GetAuthToken(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	html := string(content)

	needle := `name="authenticity_token" value="`
	idx := strings.Index(html, needle)
	if idx == -1 {
		return "", fmt.Errorf("Couldn't find authenticity_token in the HTML: %s", html)
	}
	html = html[idx + len(needle):]
	endIdx := strings.IndexRune(html, '"')
	if endIdx == -1 {
		return "", fmt.Errorf("Malformed HTML found: %s", html)
	}

	authToken := html[:endIdx]

	return authToken, nil
}
