package bbb

import (
	"net/http"
	"fmt"
	"encoding/json"
	"time"
	"strings"
	"net/http/cookiejar"
	urllib "net/url"

	"bbbchatrepo/ddp"
	"bbbchatrepo/sockjs"
)


func parseJSON(jsonstr string) interface{} {
	var parsed interface{}
	if err := json.Unmarshal([]byte(jsonstr), &parsed); err != nil {
		panic(err)
	}
	return parsed
}

type BBB struct {}

var notif Notifier
func (bbb BBB) Add(collection string, _ string, fields interface{}) {
	if collection == "group-chat-msg" {
		if fields, ok := fields.(map[string]interface{}); ok {
			if msg, ok := fields["message"]; ok {
				if msgstr, ok := msg.(string); ok {
					fmt.Println("[Chat]", msgstr)
					if strings.HasPrefix(msgstr, "!") {
						notif.SendMessage(msgstr[1:])
					}
				}
			}
		}
	}
}

func Do() {
	fmt.Println("Hello BBB")
	url := "https://bbb.jitsi.rocks/b/s0v-axz-jxu"
	http.DefaultClient.Jar, _ = cookiejar.New(nil)
	authtoken, err := GetAuthToken(url)
	if err != nil {
		panic(err)
	}
	fmt.Println("Got Authtoken:", authtoken)
	decodedURL, err := urllib.Parse(url)
	if err != nil {
		panic(err)
	}
	sessToken, err := JoinRoom(decodedURL, authtoken, "R2D2")
	fmt.Println(sessToken, err)

	sockjsURL := *decodedURL
	sockjsURL.Path = "/html5client/sockjs"
	client, err := sockjs.Connect(sockjsURL)
	if err != nil {
		panic(err)
	}

	chatinfos, err := EnterChat(*decodedURL, sessToken)
	if err != nil {
		panic(err)
	}

	ddpcon, err := ddp.Connect(WrapperClient{Wrapped: client}, &BBB{})
	if err != nil {
		panic(err)
	}
	// ddpcon.Subscribe("meteor_autoupdate_clientVersions")

	// ddpcon.CallMethod("userChangedLocalSettings", parseJSON("{\"application\":{\"animations\":false,\"chatAudioAlerts\":false,\"chatPushAlerts\":false,\"userJoinAudioAlerts\":false,\"userJoinPushAlerts\":false,\"fallbackLocale\":\"en\",\"overrideLocale\":null,\"locale\":\"en\",\"isRTL\":false},\"audio\":{\"inputDeviceId\":\"undefined\",\"outputDeviceId\":\"undefined\"},\"dataSaving\":{\"viewParticipantsWebcams\":true,\"viewScreenshare\":true}}"))

	ddpcon.CallMethod("validateAuthToken", chatinfos.MeetingID, chatinfos.InternalUserID, chatinfos.AuthToken, chatinfos.ExternUserID)

	ddpcon.Subscribe("current-user")
	authtokenres := ddpcon.CallMethod("validateAuthToken", chatinfos.MeetingID, chatinfos.InternalUserID, chatinfos.AuthToken)
	fmt.Println("Result", authtokenres)

	type sender struct {
		Id string `json:"id"`
		Name string `json:"name"`
	}

	ddpcon.Subscribe("users")
	ddpcon.Subscribe("group-chat-msg")
	ddpcon.CallMethod("startUserTyping", "public")
	fmt.Println("Msg", ddpcon.CallMethod("sendGroupChatMsg", "MAIN-PUBLIC-GROUP-CHAT", struct {
	Color string `json:"color"`
	CorrellationId string `json:"correllationId"`
	Sender sender `json:"sender"`
	Message string `json:"message"`
	} {
		Color: "0",
		CorrellationId: fmt.Sprintf("%s-%d", chatinfos.InternalUserID, time.Now().UnixNano() / 1000000),
		Sender: sender {
			Id: chatinfos.InternalUserID,
			Name: "R2D2",
		},
		Message: "zyyyz",
	}))
}
