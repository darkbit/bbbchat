package bbb

import (
	"net/http"
	urllib "net/url"
)

type SessionToken = string

func JoinRoom(url *urllib.URL, authToken string, username string) (SessionToken, error) {
	resp, err := http.PostForm(url.String(), urllib.Values { "utf8": { "✓" }, "authenticity_token": { authToken }, (url.Path + "[search]"): {""}, (url.Path + "[column]"): {""}, (url.Path + "[direction]"): {""}, (url.Path + "[join_name]"): { username } })
	if err != nil {
		return "", err
	}

	// Get the token from the final redirect request
	return resp.Request.URL.Query().Get("sessionToken"), nil
}
