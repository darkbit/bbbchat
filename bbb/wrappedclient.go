package bbb

import "bbbchatrepo/sockjs"


type WrapperClient struct {
	Wrapped *sockjs.Client
}

func (wc WrapperClient) ReadPacket() []byte {
	for {
		pkt := <-wc.Wrapped.Rx
		if data, ok := pkt.(string); ok {
			return []byte(data)
		}
	}
}

func (wc WrapperClient) WritePacket(data []byte) {
	wc.Wrapped.Tx <- string(data)
}
