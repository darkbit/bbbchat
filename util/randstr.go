package util

import (
	"math/rand"
	"encoding/base64"
)

func RandString() string {
	data := make([]byte, 9)
	rand.Read(data) // doc says, that this generates never an error
	return base64.URLEncoding.EncodeToString(data)
}
